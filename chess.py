import color
from player import Player
import pieces
import board
import game as gm
from copy import deepcopy


if __name__ == "__main__":
    board = board.Board()
    player1 = Player(color=color.Color.WHITE)
    player2 = Player(color=color.Color.BLACK)
    game = gm.Game(board, player1, player2)
    board.initialize_pieces()
    # game.play_turn_with_move("a2 a4")
    # game.play_turn_with_move("b7 b5")
    # game.play_turn_with_move("a4 b5")
    # game.play_turn_with_move("a7 a5")
    # game.play_turn_with_move("b5 a6")
    # game.play_turn_with_move("e7 e6")
    # game.play_turn_with_move("a6 a7")
    # game.play_turn_with_move("e6 e5")
    # game.play_turn_with_move("a1 a5")
    # game.play_turn_with_move("e8 e7")
    # game.play_turn_with_move("a5 d5")
    # game.play_turn_with_move("e7 e8")
    # game.play_turn_with_move("d5 a5")
    game.play_turn_with_move("e2 e4")
    game.play_turn_with_move("e7 e5")
    game.play_turn_with_move("d1 h5")
    game.play_turn()

