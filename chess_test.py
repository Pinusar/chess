from pytest import *
import color
import pieces
import board as brd
from game import Game
import player

# BOARD TESTS


def test_get_piece_by_cell():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board)
    piece = board.get_piece_by_cell(1, 1)
    assert piece == pawn


def test_get_king():
    board = brd.Board()
    pawn = pieces.Pawn(board=board)
    king = pieces.King(board=board)
    assert board.get_king(color.Color.WHITE) == king


def test_get_cell():
    cell = brd.Board.get_cell("E2")
    assert cell == (1, 4)


def test_get_chess_notation_0_0():
    notation_string = brd.Board.get_chess_notation(0, 0)
    assert notation_string == "A1"


def test_get_chess_notation_3_4():
    notation_string = brd.Board.get_chess_notation(3, 4)
    assert notation_string == "E4"


def test_get_chess_notation_7_7():
    notation_string = brd.Board.get_chess_notation(7, 7)
    assert notation_string == "H8"


def test_is_cell_under_check():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board, color.Color.BLACK)
    assert board.is_cell_under_check(0, 2, color.Color.WHITE) is True


def test_is_cell_under_check_when_not():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board, color.Color.BLACK)
    assert board.is_cell_under_check(0, 1, color.Color.WHITE) is False


def test_is_cell_under_check_when_same_color():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board, color.Color.WHITE)
    assert board.is_cell_under_check(0, 2, color.Color.WHITE) is False


# PAWN TESTS


def test_white_pawn_rank():
    board = brd.Board()
    pawn = pieces.Pawn(1, 0, board, color.Color.WHITE)
    assert pawn.rank == 2


def test_black_pawn_rank():
    board = brd.Board()
    pawn = pieces.Pawn(6, 0, board, color.Color.BLACK)
    assert pawn.rank == 2


def test_move_white_pawn_1_cell_forward():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board)
    pawn.move(2, 1)
    assert pawn.position == "(2, 1)"


def test_move_black_pawn_1_cell_forward():
    board = brd.Board()
    pawn = pieces.Pawn(2, 1, board, color.Color.BLACK)
    pawn.move(1, 1)
    assert pawn.position == "(1, 1)"


def test_move_pawn_3_cell_forward_should_fail():
    pawn = pieces.Pawn(1, 3)
    try:
        pawn.move(4, 3)
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_white_pawn_cant_move_backwards():
    pawn = pieces.Pawn(1, 1)
    try:
        pawn.move(0, 1)
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_black_pawn_cant_move_backwards():
    pawn = pieces.Pawn(1, 1, brd.Board(), color.Color.BLACK)
    try:
        pawn.move(2, 1)
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_white_pawn_cant_move_outside_of_board():
    pawn = pieces.Pawn(7, 1)
    try:
        pawn.move_forward()
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_black_pawn_cant_move_outside_of_board():
    pawn = pieces.Pawn(0, 1, brd.Board(), color.Color.BLACK)
    try:
        pawn.move_forward()
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_move_pawn_2_times():
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board)
    pawn.move_forward()
    pawn.move_forward()
    assert pawn.position == "(3, 1)"


def test_white_pawn_can_move_2_cells_from_starting_pos():
    pawn = pieces.Pawn(1, 1)
    pawn.move(3, 1)
    assert pawn.position == "(3, 1)"


def test_black_pawn_can_move_2_cells_from_starting_pos():
    pawn = pieces.Pawn(6, 1, brd.Board(), color.Color.BLACK)
    pawn.move(4, 1)
    assert pawn.position == "(4, 1)"


def test_pawn_cant_move_2_cells_from_random_pos():
    pawn = pieces.Pawn(3, 1)
    try:
        pawn.move(5, 1)
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_pawn_check_for_double_movement():
    pawn = pieces.Pawn(1, 0)
    pawn.move(3, 0)
    assert pawn.moved_double is True


def test_pawn_check_for_double_movement_when_false():
    pawn = pieces.Pawn(1, 0)
    pawn.move(2, 0)
    assert pawn.moved_double is False


def test_pawn_has_blocker():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board)
    pawn2 = pieces.Pawn(3, 1, board)
    assert pawn1.has_blocker() is True


def test_pawn_has_no_blockers():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board)
    pawn2 = pieces.Pawn(4, 1, board)
    assert pawn1.has_blocker() is False


def test_pawn_cant_move_forward_when_blocked():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board)
    pawn2 = pieces.Pawn(3, 1, board)
    try:
        pawn1.move_forward()
    except AttributeError:
        assert 1 == 1
        return
    assert 1 == 2


def test_pawn_can_capture_diagonally():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board)
    pawn2 = pieces.Pawn(3, 2, board, color.Color.BLACK)
    assert board.cells[3][2] == pawn2
    pawn1.capture(3, 2)
    assert board.cells[3][2] == pawn1


def test_black_pawn_can_capture_diagonally():
    board = brd.Board()
    pawn1 = pieces.Pawn(3, 1, board, color.Color.BLACK)
    pawn2 = pieces.Pawn(2, 2, board, color.Color.WHITE)
    assert board.cells[2][2] == pawn2
    pawn1.capture(2, 2)
    assert board.cells[2][2] == pawn1


def test_black_pawn_cant_capture_backwards():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board, color.Color.BLACK)
    pawn2 = pieces.Pawn(3, 2, board, color.Color.WHITE)
    try:
        pawn1.capture(3, 2)
    except ValueError:
        assert True
        return
    assert False


def test_pawn_cant_capture_own_color():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board, color.Color.WHITE)
    pawn2 = pieces.Pawn(3, 2, board, color.Color.WHITE)
    try:
        pawn1.capture(3, 2)
    except ValueError:
        assert 1 == 1
        return
    assert 1 == 2


def test_pawn_cant_capture_infront():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board)
    pawn2 = pieces.Pawn(3, 1, board, color.Color.BLACK)
    try:
        pawn1.capture(3, 1)
    except ValueError:
        assert 1 == 1
        return
    assert 1 == 2


def test_black_pawn_on_edge_get_targets():
    board = brd.Board()
    pawn = pieces.Pawn(6, 7, board, color.Color.BLACK)
    targets = pawn.get_targets()
    assert len(targets) == 0


def test_pawn_is_white():
    pawn = pieces.Pawn(1, 1, brd.Board(), color.Color.WHITE)
    assert pawn.is_white() is True


def test_pawn_is_white_when_false():
    pawn = pieces.Pawn(1, 1, brd.Board(), color.Color.BLACK)
    assert pawn.is_white() is False


def test_place_pawn():
    """Place method should allow to move pieces even to illegal positions."""
    board = brd.Board()
    pawn = pieces.Pawn(1, 1, board)
    pawn.place(5, 5)
    assert board.get_piece_by_cell(5, 5) == pawn


def test_white_pawn_en_passant_target():
    board = brd.Board()
    pawn1 = pieces.Pawn(4, 3, board, color.Color.WHITE)
    pawn2 = pieces.Pawn(6, 4, board, color.Color.BLACK)
    pawn2.move(4, 4)
    assert (5, 4) in pawn1.get_en_passant_targets()


def test_black_pawn_en_passant_target():
    board = brd.Board()
    pawn1 = pieces.Pawn(1, 1, board, color.Color.WHITE)
    pawn2 = pieces.Pawn(3, 2, board, color.Color.BLACK)
    pawn1.move(3, 1)
    assert (2, 1) in pawn2.get_en_passant_targets()

def test_en_passant_target_when_rules_not_met():
    board = brd.Board()
    pawn1 = pieces.Pawn(2, 1, board, color.Color.WHITE)
    pawn2 = pieces.Pawn(3, 2, board, color.Color.BLACK)
    pawn1.move(3, 1)
    assert (2, 1) not in pawn2.get_en_passant_targets()

def test_white_en_passant_capture():
    board = brd.Board()
    pawn1 = pieces.Pawn(4, 3, board, color.Color.WHITE)
    pawn2 = pieces.Pawn(6, 4, board, color.Color.BLACK)
    pawn2.move(4, 4)
    pawn1.en_passant_capture(5, 4)
    assert board.get_piece_by_cell(4, 4) is None
    assert board.get_piece_by_cell(5, 4) is pawn1
    assert pawn2 not in board.pieces
    assert board.get_piece_by_cell(4, 3) is None


def test_transform_to_queen():
    board = brd.Board()
    pawn = pieces.Pawn(7, 3, board, color.Color.WHITE)
    pawn.transform(pieces.Queen)
    assert type(pawn) is pieces.Queen
    pawn.move(0, 3)
    assert board.get_piece_by_cell(0, 3) == pawn


def test_transform_by_string_to_knight():
    board = brd.Board()
    pawn = pieces.Pawn(7, 3, board, color.Color.WHITE)
    pawn.transform_by_string("knight")
    assert type(pawn) is pieces.Knight
    pawn.move(6, 5)
    assert board.get_piece_by_cell(6, 5) == pawn

# KNIGHT TESTS

def test_knight_can_move():
    knight = pieces.Knight(0, 1)
    knight.move(2, 2)
    assert knight.position == '(2, 2)'


def test_knight_cant_move_out_of_board():
    knight = pieces.Knight(0, 1)
    try:
        knight.move(-2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_knight_cant_move_on_other_piece():
    knight = pieces.Knight(0, 1)
    pawn = pieces.Pawn(2, 2)
    try:
        knight.move(2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_knight_cant_move_too_far():
    knight = pieces.Knight(0, 1)
    try:
        knight.move(3, 2)
    except ValueError:
        assert True
        return
    assert False


def test_knight_can_capture():
    board = brd.Board()
    knight = pieces.Knight(0, 1, board, color.Color.BLACK)
    pawn = pieces.Pawn(2, 2, board, color.Color.WHITE)
    assert board.cells[2][2] == pawn
    knight.capture(2, 2)
    assert board.cells[2][2] == knight


def test_knight_cant_capture_own_color():
    board = brd.Board()
    knight = pieces.Knight(0, 1, board, color.Color.WHITE)
    pawn = pieces.Pawn(2, 2, board, color.Color.WHITE)
    try:
        knight.capture(2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_knight_cant_capture_empty_cell():
    board = brd.Board()
    knight = pieces.Knight(0, 1, board, color.Color.WHITE)
    try:
        knight.capture(2, 2)
    except ValueError:
        assert True
        return
    assert False


# BISHOP TESTS

def test_bishop_can_move():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board)
    assert board.cells[0][2] == bishop
    bishop.move(2, 4)
    assert board.cells[0][2] is None
    assert board.cells[2][4] == bishop


def test_bishop_cant_move_when_blocked():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board)
    pawn = pieces.Pawn(1, 3, board)
    try:
        bishop.move(2, 4)
    except ValueError:
        assert True
        return
    assert False


def test_bishop_cant_move_straight():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board)
    try:
        bishop.move(2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_bishop_can_capture():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board, color.Color.BLACK)
    pawn = pieces.Pawn(2, 4, board, color.Color.WHITE)
    assert board.cells[2][4] == pawn
    bishop.capture(2, 4)
    assert board.cells[2][4] == bishop


def test_bishop_cant_capture_own_color():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board, color.Color.WHITE)
    pawn = pieces.Pawn(2, 4, board, color.Color.WHITE)
    try:
        bishop.capture(2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_bishop_cant_capture_empty_cell():
    board = brd.Board()
    bishop = pieces.Bishop(0, 2, board, color.Color.WHITE)
    try:
        bishop.capture(2, 4)
    except ValueError:
        assert True
        return
    assert False


# ROOK TESTS

def test_rook_can_move():
    board = brd.Board()
    rook = pieces.Rook(0, 0, board)
    assert board.cells[0][0] == rook
    rook.move(0, 4)
    assert board.cells[0][0] is None
    assert board.cells[0][4] == rook


def test_rook_cant_move_when_blocked():
    board = brd.Board()
    rook = pieces.Rook(0, 0, board)
    pawn = pieces.Pawn(1, 0, board)
    try:
        rook.move(2, 0)
    except ValueError:
        assert True
        return
    assert False


def test_rook_cant_move_diagonally():
    board = brd.Board()
    rook = pieces.Rook(0, 0, board)
    try:
        rook.move(1, 1)
    except ValueError:
        assert True
        return
    assert False


def test_rook_can_capture():
    board = brd.Board()
    rook = pieces.Rook(0, 4, board, color.Color.BLACK)
    pawn = pieces.Pawn(2, 4, board, color.Color.WHITE)
    assert board.cells[2][4] == pawn
    rook.capture(2, 4)
    assert board.cells[2][4] == rook


def test_rook_cant_capture_own_color():
    board = brd.Board()
    rook = pieces.Rook(0, 4, board, color.Color.WHITE)
    pawn = pieces.Pawn(2, 4, board, color.Color.WHITE)
    try:
        rook.capture(2, 2)
    except ValueError:
        assert True
        return
    assert False


def test_rook_cant_capture_empty_cell():
    board = brd.Board()
    rook = pieces.Rook(0, 4, board, color.Color.WHITE)
    try:
        rook.capture(2, 4)
    except ValueError:
        assert True
        return
    assert False


# QUEEN TESTS

def test_queen_can_move_straight():
    board = brd.Board()
    queen = pieces.Queen(0, 0, board)
    assert board.cells[0][0] == queen
    queen.move(0, 4)
    assert board.cells[0][0] is None
    assert board.cells[0][4] == queen


def test_queen_can_move_diagonally():
    board = brd.Board()
    queen = pieces.Queen(0, 2, board)
    assert board.cells[0][2] == queen
    queen.move(2, 4)
    assert board.cells[0][2] is None
    assert board.cells[2][4] == queen


def test_queen_cant_move_when_blocked():
    board = brd.Board()
    queen = pieces.Queen(0, 0, board)
    pawn = pieces.Pawn(1, 0, board)
    try:
        queen.move(2, 0)
    except ValueError:
        assert True
        return
    assert False


def test_queen_cant_move_like_knight():
    board = brd.Board()
    queen = pieces.Queen(0, 0, board)
    try:
        queen.move(2, 1)
    except ValueError:
        assert True
        return
    assert False


# KING TESTS

def test_king_can_move_diagonally():
    board = brd.Board()
    king = pieces.King(0, 0, board)
    assert board.cells[0][0] == king
    king.move(1, 1)
    assert board.cells[0][0] is None
    assert board.cells[1][1] == king


def test_king_can_capture():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.BLACK)
    pawn = pieces.Pawn(1, 4, board, color.Color.WHITE)
    assert board.cells[1][4] == pawn
    king.capture(1, 4)
    assert board.cells[1][4] == king


def test_king_cant_capture_own_color():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    pawn = pieces.Pawn(1, 4, board, color.Color.WHITE)
    try:
        king.capture(1, 4)
    except ValueError:
        assert True
        return
    assert False


def test_king_can_execute_castle_kingside():
    game_board = brd.Board()
    king = pieces.King(0, 4, game_board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, game_board, color.Color.WHITE)
    king.castle(rook)
    assert game_board.cells[0][6] == king
    assert game_board.cells[0][4] is None
    assert game_board.cells[0][5] == rook
    assert game_board.cells[0][7] is None


def test_king_can_execute_castle_queenside():
    game_board = brd.Board()
    king = pieces.King(0, 4, game_board, color.Color.WHITE)
    rook = pieces.Rook(0, 0, game_board, color.Color.WHITE)
    king.castle(rook)
    assert game_board.cells[0][2] == king
    assert game_board.cells[0][4] is None
    assert game_board.cells[0][3] == rook
    assert game_board.cells[0][0] is None


def test_king_can_execute_castle_queenside():
    game_board = brd.Board()
    king = pieces.King(7, 4, game_board, color.Color.BLACK)
    rook = pieces.Rook(7, 0, game_board, color.Color.BLACK)
    king.castle(rook)
    assert game_board.cells[7][2] == king
    assert game_board.cells[7][4] is None
    assert game_board.cells[7][3] == rook
    assert game_board.cells[7][0] is None


def test_king_cant_execute_castle_if_no_rook():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = None
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_king_cant_execute_castle_if_rook_not_in_corner():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 6, board, color.Color.WHITE)
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_king_cant_execute_castle_if_rook_wrong_color():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.BLACK)
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_king_cant_execute_castle_if_path_is_blocked():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    pawn = pieces.Pawn(0, 6, board, color.Color.WHITE)
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_king_cant_execute_castle_when_under_check():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    checking_bishop = pieces.Bishop(2, 2, board, color.Color.BLACK)
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_king_cant_execute_castle_through_pawn_check():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    pawn = pieces.Pawn(1, 4, board, color.Color.BLACK)
    try:
        king.castle(rook)
    except ValueError:
        assert True
        return
    assert False


def test_can_castle_kingside():
    game_board = brd.Board()
    king = pieces.King(0, 4, game_board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, game_board, color.Color.WHITE)
    assert king.can_castle(rook) is True


def test_can_castle_queenside():
    game_board = brd.Board()
    king = pieces.King(0, 4, game_board, color.Color.WHITE)
    rook = pieces.Rook(0, 0, game_board, color.Color.WHITE)
    assert king.can_castle(rook) is True


def test_cant_castle_if_no_rook():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = None
    assert king.can_castle(rook) is False


def test_cant_castle_if_rook_not_in_corner():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 6, board, color.Color.WHITE)
    assert king.can_castle(rook) is False


def test_cant_castle_if_rook_wrong_color():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.BLACK)
    assert king.can_castle(rook) is False


def test_cant_castle_if_path_is_blocked():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    pawn = pieces.Pawn(0, 6, board, color.Color.WHITE)
    assert king.can_castle(rook) is False


def test_cant_castle_when_under_check():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    checking_bishop = pieces.Bishop(2, 2, board, color.Color.BLACK)
    assert king.can_castle(rook) is False


def test_cant_castle_through_pawn_check():
    board = brd.Board()
    king = pieces.King(0, 4, board, color.Color.WHITE)
    rook = pieces.Rook(0, 7, board, color.Color.WHITE)
    pawn = pieces.Pawn(1, 4, board, color.Color.BLACK)
    assert king.can_castle(rook) is False


def test_target_rook_by_target_cell_c8():
    board = brd.Board()
    king = pieces.King(7, 4, board, color.Color.BLACK)
    rook = pieces.Rook(7, 0, board, color.Color.BLACK)
    assert king.get_target_rook_by_target_cell(7, 2) == rook
    assert king.can_castle(king.get_target_rook_by_target_cell(7, 2)) is True
    assert type(king) is pieces.King and king.can_castle(king.get_target_rook_by_target_cell(7, 2)) is True
    assert type(king) is not pieces.King or (not 2 == 6 and not 2 == 2) or not king.can_castle(king.get_target_rook_by_target_cell(7, 2)) is False


# GAME TESTS

def test_is_under_check():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    board.initialize_pieces()
    kingpawn = board.get_piece_by_cell(1, 5)
    kingpawn.move(3, 5)
    blackpawn = board.get_piece_by_cell(6, 4)
    blackpawn.move(4, 4)
    queen = board.get_piece_by_cell(7, 3)
    queen.move(3, 7)
    assert game.is_under_check(player1) is True


def test_active_player():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    assert game.active_player == player1


def test_move_pattern_ok_when_a2_a4():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    assert game.input_matches_move_pattern("a2 a4") is True


def test_move_pattern_false_when_a2_i4():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    assert game.input_matches_move_pattern("a2 i4") is False


def test_move_pattern_false_when_a1_a9():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    assert game.input_matches_move_pattern("a1 a9") is False


def test_move_pattern_false_when_hello():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    assert game.input_matches_move_pattern("hello") is False


def test_play_turn_castle_kingside():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    board.initialize_pieces()
    game.play_turn_with_move("e2 e4")
    game.play_turn_with_move("e7 e5")
    game.play_turn_with_move("a2 a3")
    game.play_turn_with_move("g8 f6")
    game.play_turn_with_move("a3 a4")
    game.play_turn_with_move("f8 c5")
    game.play_turn_with_move("a4 a5")
    game.play_turn_with_move("e8 g8")
    assert type(board.get_piece_by_cell(7, 6)) is pieces.King
    assert type(board.get_piece_by_cell(7, 5)) is pieces.Rook


def test_play_turn_castle_queenside():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    board.initialize_pieces()
    game.play_turn_with_move("e2 e4")
    game.play_turn_with_move("e7 e5")
    game.play_turn_with_move("a2 a3")
    game.play_turn_with_move("d8 h4")
    game.play_turn_with_move("a3 a4")
    game.play_turn_with_move("b8 c6")
    game.play_turn_with_move("a4 a5")
    game.play_turn_with_move("d7 d6")
    game.play_turn_with_move("a1 a2")
    game.play_turn_with_move("c8 d7")
    game.play_turn_with_move("a2 a1")
    game.play_turn_with_move("e8 c8")
    assert type(board.get_piece_by_cell(7, 2)) is pieces.King
    assert type(board.get_piece_by_cell(7, 3)) is pieces.Rook


def test_play_turn_en_passant_capture():
    board = brd.Board()
    player1 = player.Player(color=color.Color.WHITE)
    player2 = player.Player(color=color.Color.BLACK)
    game = Game(board, player1, player2)
    board.initialize_pieces()
    game.play_turn_with_move("a2 a4")
    game.play_turn_with_move("b7 b5")
    game.play_turn_with_move("a4 b5")
    game.play_turn_with_move("a7 a5")
    game.play_turn_with_move("b5 a6")
    assert type(board.get_piece_by_cell(5, 0)) is pieces.Pawn
    assert board.get_piece_by_cell(4, 0) is None
    assert board.get_piece_by_cell(4, 1) is None



