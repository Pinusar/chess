import color
from copy import deepcopy
import player
import board
import pieces
import re


class Game:

    def __init__(self, board, white_player=player.Player(), black_player=player.Player(color=color.Color.BLACK)):
        self.board = board
        self.white_player = white_player
        self.black_player = black_player
        self.active_player = white_player
        self.move_history = []
        self.turn_count = 1

    def reset(self):
        self.board.initialize_pieces()
        self.active_player = self.white_player
        self.move_history = []
        self.turn_count = 1

    def play_turn(self):
        self.board.reset_pawn_double_movement_flag()
        print(f"Turn {self.turn_count}. {self.active_player.color} player turn.")
        move = input("Please enter your move (e.g. a2 a4): ")
        while (move.lower() != "resign" and (not self.input_matches_move_pattern(move))) or self.is_move_input_illegal(move):
            move = input("Invalid move. Please enter your move again (e.g. a2 a4): ")
        return self.play_turn_with_move(move)

    def play_turn_with_move(self, move):
        if (move.lower() != "resign" and (not self.input_matches_move_pattern(move))) or self.is_move_input_illegal(move):
            raise ValueError("Invalid move!")
        elif move.lower() == "resign":
            print(f"GAME OVER! {self.active_player} lost!")
            return False
        else:
            parts = move.split(" ")
            starting_cell = board.Board.get_cell(parts[0])
            target_cell = board.Board.get_cell(parts[1])
            self.move_history.append(f"{self.turn_count}. {move}")
            self.execute_move_or_capture(starting_cell, target_cell)
            piece = self.board.get_piece_by_cell(target_cell[0], target_cell[1])
            if type(piece) is pieces.Pawn and piece.rank == 8:
                piece.promote()
            if self.active_player.color == color.Color.BLACK:
                self.turn_count += 1
            self.switch_active_player()
            print(self.board.get_minimalistic_repr())
            return True

    def input_matches_move_pattern(self, input):
        move_pattern = re.compile(r"[a-hA-H][1-8] [a-hA-H][1-8]")
        return bool(re.match(move_pattern, input))

    def is_move_illegal(self, starting_cell, target_cell):
        starting_x = starting_cell[0]
        starting_y = starting_cell[1]
        target_x = target_cell[0]
        target_y = target_cell[1]
        if not self.board.has_piece(starting_x, starting_y):
            return True
        piece = self.board.get_piece_by_cell(starting_x, starting_y)
        if piece.color != self.active_player.color:
            return True
        if self.board.has_piece(target_x, target_y) and self.board.get_piece_by_cell(target_x, target_y).color == piece.color:
            return True
        if type(piece) is pieces.Pawn:
            if self.board.has_piece(target_x, target_y) and target_cell not in piece.get_targets():
                return True
            if (target_cell not in piece.get_moves()) and target_cell not in piece.get_targets():
                return True
        elif (target_cell not in piece.get_moves()) and target_cell not in piece.get_targets() and (
                type(piece) is not pieces.King or (not target_y == 6 and not target_y == 2) or not piece.can_castle(piece.get_target_rook_by_target_cell(target_x, target_y))):
            return True
        if self.move_would_put_player_under_check(starting_cell, target_cell):
            return True
        return False

    def is_move_input_illegal(self, move):
        """Overload is_move_illegal."""
        if move.lower() == "resign":
            return False
        parts = move.split(" ")
        starting_cell = board.Board.get_cell(parts[0])
        target_cell = board.Board.get_cell(parts[1])
        return self.is_move_illegal(starting_cell, target_cell)

    def execute_move_or_capture(self, starting_cell, target_cell):
        starting_x = starting_cell[0]
        starting_y = starting_cell[1]
        target_x = target_cell[0]
        target_y = target_cell[1]
        piece = self.board.get_piece_by_cell(starting_x, starting_y)
        if self.board.has_piece(target_x, target_y):
            piece.capture(target_x, target_y)
        elif type(piece) is pieces.Pawn and target_cell in piece.get_en_passant_targets():
            piece.capture(target_x, target_y)
        elif type(piece) is pieces.King and piece.can_castle(piece.get_target_rook_by_target_cell(target_x, target_y)):
            target_rook = piece.get_target_rook_by_target_cell(target_x, target_y)
            piece.castle(target_rook)
        else:
            piece.move(target_x, target_y)

    def switch_active_player(self):
        if self.active_player.color == color.Color.WHITE:
            self.active_player = self.black_player
        else:
            self.active_player = self.white_player

    def is_under_check(self, player):
        target_king = self.board.get_king(player.color)
        return self.board.is_cell_under_check(target_king.x, target_king.y, target_king.color)

    def move_would_put_player_under_check(self, starting_cell, target_cell):
        simulation = deepcopy(self)
        simulation.execute_move_or_capture(starting_cell, target_cell)
        active_player = simulation.active_player
        if simulation.is_under_check(active_player):
            return True
        else:
            return False

    def start_new_game(self):
        self.reset()
        game_not_over = True
        print(self.board.get_minimalistic_repr())
        while game_not_over is True:
            game_not_over = self.play_turn()
        for game_move in self.move_history:
            print(game_move)
