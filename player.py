import color


class Player:

    def __init__(self, color=color.Color.WHITE, name="Default"):
        self.color = color
        self.name = name

    def __str__(self):
        return f"{self.color} player"
