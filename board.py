import color
import pieces


class Board:

    def __init__(self):
        self.pieces = []
        self.cells = []
        for i in range(8):
            self.cells.append([])
            for j in range(8):
                self.cells[i].append(None)

    def __str__(self):
        rowcount = 1
        result = "  A  B  C  D  E  F  G  H"
        for row in self.cells:
            result += "\n" + str(rowcount)
            for cell in row:
                if cell is None:
                    result += "[ ]"
                elif type(cell) is pieces.Pawn:
                    result += "[P]"
                elif type(cell) is pieces.Knight:
                    result += "[N]"
                elif type(cell) is pieces.Bishop:
                    result += "[B]"
                elif type(cell) is pieces.Rook:
                    result += "[R]"
                elif type(cell) is pieces.Queen:
                    result += "[Q]"
                elif type(cell) is pieces.King:
                    result += "[K]"
                else:
                    raise ValueError("Invalid figure class on board.")
            rowcount += 1
        return result

    def get_minimalistic_repr(self):
        rowcount = 1
        result = "  A  B  C  D  E  F  G  H"
        for row in self.cells:
            result += "\n" + str(rowcount)
            for cell in row:
                result += self.get_piece_repr(cell)
            rowcount += 1
        result += "\n"
        return result

    @staticmethod
    def get_piece_repr(cell):
        if cell is None:
            return " . "
        else:
            return f" {cell.get_letter} "

    @staticmethod
    def get_piece_gui_repr(cell):
        if cell is None:
            return "   "
        else:
            return f" {cell.get_letter} "

    def has_piece(self, x, y):
        if not Board.cell_within_limits(x, y):
            return False
        if self.cells[x][y]:
            return True
        else:
            return False

    def clear_cell(self, x, y):
        self.cells[x][y] = None

    def clear_all_cells(self):
        for i in range(8):
            for j in range(8):
                self.clear_cell(i, j)

    def get_piece_by_cell(self, x, y):
        return self.cells[x][y]

    def get_king(self, color):
        king = None
        for piece in self.pieces:
            if type(piece) is pieces.King and piece.color == color:
                king = piece
        return king

    def remove_all_pieces(self):
        self.pieces.clear()

    def clear_board(self):
        self.remove_all_pieces()
        self.clear_all_cells()

    def initialize_pieces(self):
        self.clear_board()
        white_rook1 = pieces.Rook(0, 0, self)
        white_rook2 = pieces.Rook(0, 7, self)
        white_knight1 = pieces.Knight(0, 1, self)
        white_knight2 = pieces.Knight(0, 6, self)
        white_bishop1 = pieces.Bishop(0, 2, self)
        white_bishop2 = pieces.Bishop(0, 5, self)
        white_queen = pieces.Queen(0, 3, self)
        white_king = pieces.King(0, 4, self)
        for i in range(8):
            white_pawn = pieces.Pawn(1, i, self)
        black_rook1 = pieces.Rook(7, 0, self, color.Color.BLACK)
        black_rook2 = pieces.Rook(7, 7, self, color.Color.BLACK)
        black_knight1 = pieces.Knight(7, 1, self, color.Color.BLACK)
        black_knight2 = pieces.Knight(7, 6, self, color.Color.BLACK)
        black_bishop1 = pieces.Bishop(7, 2, self, color.Color.BLACK)
        black_bishop2 = pieces.Bishop(7, 5, self, color.Color.BLACK)
        black_queen = pieces.Queen(7, 3, self, color.Color.BLACK)
        black_king = pieces.King(7, 4, self, color.Color.BLACK)
        for i in range(8):
            black_pawn = pieces.Pawn(6, i, self, color.Color.BLACK)

    def is_cell_under_check(self, x, y, color):
        target_cell = (x, y)
        if not self.has_piece(x, y):
            dummy_pawn = pieces.Pawn(x, y, self)
            for piece in self.pieces:
                if piece.color != color and target_cell in piece.get_targets():
                    print(f"Under check from cell {Board.get_chess_notation(piece.x, piece.y)}")
                    self.clear_cell(x, y)
                    return True
            self.clear_cell(x, y)
        else:
            for piece in self.pieces:
                if piece.color != color and target_cell in piece.get_targets():
                    print(f"Under check from cell {Board.get_chess_notation(piece.x, piece.y)}")
                    return True
        return False

    def reset_pawn_double_movement_flag(self):
        for piece in self.pieces:
            if type(piece) is pieces.Pawn:
                if piece.moved_double and piece.moved_double_flag:
                    piece.moved_double_flag = False
                else:
                    piece.moved_double = False
                    piece.moved_double_flag = False

    @staticmethod
    def get_cell(str):
        """Method to accept standard chess notation like A1, E6, etc.
        A1 should return 0, 0
        C3, 2, 2
        """
        letter = str[0].upper()
        number = int(str[1])
        letter_dict = {"A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5, "G": 6, "H": 7}
        x = number - 1
        y = letter_dict[letter.upper()]
        return (x, y)

    @staticmethod
    def get_chess_notation(x, y):
        letter_dict = {0: "A", 1: "B", 2: "C", 3: "D", 4: "E", 5: "F", 6: "G", 7: "H"}
        letter = letter_dict[y]
        number = str(x + 1)
        return letter + number

    @staticmethod
    def cell_within_limits(x, y):
        return 0 <= x <= 7 and 0 <= y <= 7

