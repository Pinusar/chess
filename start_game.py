import player as plr
import color as clr
import pieces
import board as brd
import game as gm

board = brd.Board()
player1 = plr.Player(color=clr.Color.WHITE)
player2 = plr.Player(color=clr.Color.BLACK)
game = gm.Game(board, player1, player2)
game.start_new_game()