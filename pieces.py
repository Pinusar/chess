from abc import ABC, abstractmethod
import board
import color


class Piece(ABC):

    def __init__(self, board=board.Board()):
        self.board = board

    @abstractmethod
    def get_moves(self):
        pass

    def move(self, x, y):
        if (x, y) in self.get_moves():
            self.board.clear_cell(self.x, self.y)
            self.x = x
            self.y = y
            self.board.cells[x][y] = self
            self.has_moved = True
        else:
            raise ValueError("Invalid move!")

    @abstractmethod
    def get_targets(self):
        pass

    def capture(self, x, y):
        targets = self.get_targets()
        if (x, y) in targets:
            target_piece = self.board.get_piece_by_cell(x, y)
            self.board.clear_cell(x, y)
            self.board.pieces.remove(target_piece)
            self.board.clear_cell(self.x, self.y)
            self.x = x
            self.y = y
            self.board.cells[x][y] = self
        else:
            raise ValueError("Illegal capture move")

    def is_white(self):
        return self.color == color.Color.WHITE

    def place(self, x, y):
        self.board.clear_cell(self.x, self.y)
        self.x = x
        self.y = y
        self.board.cells[x][y] = self


class Pawn(Piece):

    def __init__(self, x=0, y=1, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color
        self.moved_double = False
        self.moved_double_flag = False

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def rank(self):
        if self.color == color.Color.WHITE:
            return self.x + 1
        else:
            return 8 - self.x

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "P"
        else:
            return "p"

    def __str__(self):
        return f"{self.color} pawn at {self.position}"

    def __repr__(self):
        return f"{self.color} pawn at {self.position}"

    def is_white(self):
        return self.color == color.Color.WHITE

    def get_moves(self):
        if self.color == color.Color.BLACK:
            available_moves = self.get_black_moves()
        else:
            available_moves = self.get_white_moves()
        return available_moves

    def get_white_moves(self):
        available_moves = []
        if self.x < 7 and not self.has_blocker():
            if self.x == 1:
                available_moves.append((self.x + 2, self.y))
            available_moves.append((self.x + 1, self.y))
        return available_moves

    def get_black_moves(self):
        available_moves = []
        if self.x > 0 and not self.has_blocker():
            if self.x == 6:
                available_moves.append((self.x - 2, self.y))
            available_moves.append((self.x - 1, self.y))
        return available_moves

    def has_blocker(self):
        if self.color == color.Color.WHITE:
            return self.has_blocker_white()
        else:
            return self.has_blocker_black()

    def has_blocker_white(self):
        if self.board.cells[self.x + 1][self.y]:
            return True
        else:
            return False

    def has_blocker_black(self):
        if self.board.cells[self.x - 1][self.y]:
            return True
        else:
            return False

    def get_targets(self):
        targets = []
        if self.color == color.Color.WHITE:
            targets = self.get_targets_white()
        else:
            targets = self.get_targets_black()
        en_passant_targets = self.get_en_passant_targets()
        targets.extend(en_passant_targets)
        return targets

    def get_targets_white(self):
        targets = []
        if self.board.has_piece(self.x + 1, self.y - 1) and self.board.cells[self.x + 1][
            self.y - 1].color != self.color:
            targets.append((self.x + 1, self.y - 1))
        if self.board.has_piece(self.x + 1, self.y + 1) and self.board.cells[self.x + 1][
            self.y + 1].color != self.color:
            targets.append((self.x + 1, self.y + 1))
        return targets

    def get_targets_black(self):
        targets = []
        if self.board.has_piece(self.x - 1, self.y - 1) and self.board.cells[self.x - 1][
            self.y - 1].color != self.color:
            targets.append((self.x - 1, self.y - 1))
        if self.board.has_piece(self.x - 1, self.y + 1) and self.board.cells[self.x - 1][
            self.y + 1].color != self.color:
            targets.append((self.x - 1, self.y + 1))
        return targets

    def get_en_passant_targets(self):
        targets = []
        if self.rank == 5:
            if self.board.has_piece(self.x, self.y - 1) and self.board.cells[self.x][
                self.y - 1].color != self.color and type(self.board.cells[self.x][self.y - 1]) is Pawn and \
                    self.board.cells[self.x][self.y - 1].moved_double is True:
                if self.color is color.Color.WHITE:
                    targets.append((self.x + 1, self.y - 1))
                else:
                    targets.append((self.x - 1, self.y - 1))
            if self.board.has_piece(self.x, self.y + 1) and self.board.cells[self.x][
                self.y + 1].color != self.color and type(self.board.cells[self.x][self.y + 1]) is Pawn and \
                    self.board.cells[self.x][self.y + 1].moved_double is True:
                if self.color is color.Color.WHITE:
                    targets.append((self.x + 1, self.y + 1))
                else:
                    targets.append((self.x - 1, self.y + 1))
        return targets

    def move(self, x, y):
        if self.color == color.Color.WHITE:
            available_moves = self.get_moves()
        else:
            available_moves = self.get_black_moves()
        if (x, y) in available_moves:
            self.check_for_double_movement(x, y)
            self.board.clear_cell(self.x, self.y)
            self.x = x
            self.y = y
            self.board.cells[x][y] = self
        else:
            raise AttributeError("Invalid move!")

    def check_for_double_movement(self, x, y):
        if abs(self.x - x) == 2:
            self.moved_double = True
            self.moved_double_flag = True
        else:
            pass

    def move_forward(self):
        if self.color == color.Color.WHITE:
            self.move(self.x + 1, self.y)
        else:
            self.move(self.x - 1, self.y)

    def capture(self, x, y):
        en_passant_targets = self.get_en_passant_targets()
        if (x, y) in en_passant_targets:
            self.en_passant_capture(x, y)
        else:
            targets = self.get_targets()
            if (x, y) in targets:
                target_piece = self.board.get_piece_by_cell(x, y)
                self.board.clear_cell(x, y)
                self.board.pieces.remove(target_piece)
                self.board.clear_cell(self.x, self.y)
                self.x = x
                self.y = y
                self.board.cells[x][y] = self
            else:
                raise ValueError("Illegal capture move")

    def en_passant_capture(self, x, y):
        if (x, y) in self.get_en_passant_targets():
            if self.color == color.Color.WHITE:
                target_piece_x = x - 1
                target_piece = self.board.get_piece_by_cell(target_piece_x, y)
            else:
                target_piece_x = x + 1
                target_piece = self.board.get_piece_by_cell(target_piece_x, y)
            self.board.clear_cell(target_piece_x, y)
            self.board.pieces.remove(target_piece)
            self.board.clear_cell(self.x, self.y)
            self.x = x
            self.y = y
            self.board.cells[x][y] = self
        else:
            raise ValueError("Illegal capture move")

    def transform(self, piece_type):
        self.__class__ = piece_type

    def transform_by_string(self, type_string):
        type_string = type_string.lower()
        if type_string == "queen":
            self.transform(Queen)
        elif type_string == "knight":
            self.transform(Knight)
        elif type_string == "rook":
            self.transform(Rook)
        elif type_string == "bishop":
            self.transform(Bishop)
        else:
            raise ValueError("Invalid transform type!")

    def promote(self):
        x = self.x
        y = self.y
        allowed_types = ["queen", "knight", "rook", "bishop"]
        new_type = input(f"Please enter type to transform the pawn at {board.Board.get_chess_notation(x, y)}: ")
        new_type = new_type.lower()
        while new_type not in allowed_types:
            new_type = input(
                f"Invalid type entered. Please enter a valid type to transform the pawn at {board.Board.get_chess_notation(x, y)}: ")
        self.transform_by_string(new_type)
        return


class Knight(Piece):

    def __init__(self, x=0, y=1, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "N"
        else:
            return "n"

    def __str__(self):
        return f"{self.color} knight at {self.position}"

    def __repr__(self):
        return f"{self.color} knight at {self.position}"

    def get_moves(self):
        move_candidates = self.get_move_candidates()
        available_moves = []
        for move in move_candidates:
            if board.Board.cell_within_limits(move[0], move[1]) and not self.board.has_piece(move[0], move[1]):
                available_moves.append(move)
        return available_moves

    def get_move_candidates(self):
        move_candidates = [(self.x - 1, self.y - 2), (self.x - 2, self.y - 1), (self.x - 2, self.y + 1),
                           (self.x - 1, self.y + 2), (self.x + 1, self.y + 2), (self.x + 2, self.y + 1),
                           (self.x + 2, self.y - 1), (self.x + 1, self.y - 2)]
        return move_candidates

    def get_targets(self):
        available_targets = []
        possible_cells = self.get_move_candidates()
        for cell in possible_cells:
            if board.Board.cell_within_limits(cell[0], cell[1]) and self.board.has_piece(cell[0], cell[1]) and \
                    self.board.cells[cell[0]][cell[1]].color != self.color:
                available_targets.append(cell)
        return available_targets


class Bishop(Piece):

    def __init__(self, x=0, y=2, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "B"
        else:
            return "b"

    def __str__(self):
        return f"{self.color} bishop at {self.position}"

    def __repr__(self):
        return f"{self.color} bishop at {self.position}"

    def get_moves(self):
        moves = []
        for move in self.get_nw_moves():
            moves.append(move)
        for move in self.get_ne_moves():
            moves.append(move)
        for move in self.get_se_moves():
            moves.append(move)
        for move in self.get_sw_moves():
            moves.append(move)
        return moves

    def get_nw_moves(self):
        """Get moves in Northwest direction."""
        moves = self.get_moves_helper(-1, -1)
        return moves

    def get_ne_moves(self):
        """Get moves in Northeast direction."""
        moves = self.get_moves_helper(-1, 1)
        return moves

    def get_se_moves(self):
        """Get moves in Southeast direction."""
        moves = self.get_moves_helper(1, 1)
        return moves

    def get_sw_moves(self):
        """Get moves in Southwest direction."""
        moves = self.get_moves_helper(1, -1)
        return moves

    def get_moves_helper(self, x_increment, y_increment):
        moves = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y) and not self.board.has_piece(next_x, next_y):
            moves.append(next_cell)
            next_x = next_x + x_increment
            next_y = next_y + y_increment
            next_cell = (next_x, next_y)
        return moves

    def get_targets(self):
        targets = []
        for target in self.get_nw_targets():
            targets.append(target)
        for target in self.get_ne_targets():
            targets.append(target)
        for target in self.get_se_targets():
            targets.append(target)
        for target in self.get_sw_targets():
            targets.append(target)
        return targets

    def get_nw_targets(self):
        targets = self.get_targets_helper(-1, -1)
        return targets

    def get_ne_targets(self):
        targets = self.get_targets_helper(-1, 1)
        return targets

    def get_se_targets(self):
        targets = self.get_targets_helper(1, 1)
        return targets

    def get_sw_targets(self):
        targets = self.get_targets_helper(1, -1)
        return targets

    def get_targets_helper(self, x_increment, y_increment):
        targets = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y):
            if self.board.has_piece(next_x, next_y):
                if self.board.cells[next_x][next_y].color != self.color:
                    targets.append(next_cell)
                break
            next_x += x_increment
            next_y += y_increment
            next_cell = (next_x, next_y)
        return targets


class Rook(Piece):

    def __init__(self, x=0, y=0, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color
        self.has_moved = False

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "R"
        else:
            return "r"

    def __str__(self):
        return f"{self.color} rook at {self.position}"

    def __repr__(self):
        return f"{self.color} rook at {self.position}"

    def get_moves(self):
        moves = []
        for move in self.get_n_moves():
            moves.append(move)
        for move in self.get_e_moves():
            moves.append(move)
        for move in self.get_s_moves():
            moves.append(move)
        for move in self.get_w_moves():
            moves.append(move)
        return moves

    def get_n_moves(self):
        """Get moves in North direction."""
        moves = self.get_moves_helper(-1, 0)
        return moves

    def get_e_moves(self):
        """Get moves in East direction."""
        moves = self.get_moves_helper(0, 1)
        return moves

    def get_s_moves(self):
        """Get moves in South direction."""
        moves = self.get_moves_helper(1, 0)
        return moves

    def get_w_moves(self):
        """Get moves in West direction."""
        moves = self.get_moves_helper(0, -1)
        return moves

    def get_moves_helper(self, x_increment, y_increment):
        moves = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y) and not self.board.has_piece(next_x, next_y):
            moves.append(next_cell)
            next_x = next_x + x_increment
            next_y = next_y + y_increment
            next_cell = (next_x, next_y)
        return moves

    def get_targets(self):
        targets = []
        for target in self.get_n_targets():
            targets.append(target)
        for target in self.get_e_targets():
            targets.append(target)
        for target in self.get_s_targets():
            targets.append(target)
        for target in self.get_w_targets():
            targets.append(target)
        return targets

    def get_n_targets(self):
        targets = self.get_targets_helper(-1, 0)
        return targets

    def get_e_targets(self):
        targets = self.get_targets_helper(0, 1)
        return targets

    def get_s_targets(self):
        targets = self.get_targets_helper(1, 0)
        return targets

    def get_w_targets(self):
        targets = self.get_targets_helper(0, -1)
        return targets

    def get_targets_helper(self, x_increment, y_increment):
        targets = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y):
            if self.board.has_piece(next_x, next_y):
                if self.board.cells[next_x][next_y].color != self.color:
                    targets.append(next_cell)
                break
            next_x += x_increment
            next_y += y_increment
            next_cell = (next_x, next_y)
        return targets


class Queen(Piece):

    def __init__(self, x=0, y=3, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "Q"
        else:
            return "q"

    def __str__(self):
        return f"{self.color} queen at {self.position}"

    def __repr__(self):
        return f"{self.color} queen at {self.position}"

    def get_moves(self):
        moves = []
        for move in self.get_n_moves():
            moves.append(move)
        for move in self.get_e_moves():
            moves.append(move)
        for move in self.get_s_moves():
            moves.append(move)
        for move in self.get_w_moves():
            moves.append(move)
        for move in self.get_nw_moves():
            moves.append(move)
        for move in self.get_ne_moves():
            moves.append(move)
        for move in self.get_se_moves():
            moves.append(move)
        for move in self.get_sw_moves():
            moves.append(move)
        return moves

    def get_n_moves(self):
        """Get moves in North direction."""
        moves = self.get_moves_helper(-1, 0)
        return moves

    def get_e_moves(self):
        """Get moves in East direction."""
        moves = self.get_moves_helper(0, 1)
        return moves

    def get_s_moves(self):
        """Get moves in South direction."""
        moves = self.get_moves_helper(1, 0)
        return moves

    def get_w_moves(self):
        """Get moves in West direction."""
        moves = self.get_moves_helper(0, -1)
        return moves

    def get_nw_moves(self):
        """Get moves in Northwest direction."""
        moves = self.get_moves_helper(-1, -1)
        return moves

    def get_ne_moves(self):
        """Get moves in Northeast direction."""
        moves = self.get_moves_helper(-1, 1)
        return moves

    def get_se_moves(self):
        """Get moves in Southeast direction."""
        moves = self.get_moves_helper(1, 1)
        return moves

    def get_sw_moves(self):
        """Get moves in Southwest direction."""
        moves = self.get_moves_helper(1, -1)
        return moves

    def get_moves_helper(self, x_increment, y_increment):
        moves = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y) and not self.board.has_piece(next_x, next_y):
            moves.append(next_cell)
            next_x = next_x + x_increment
            next_y = next_y + y_increment
            next_cell = (next_x, next_y)
        return moves

    def get_targets(self):
        targets = []
        for target in self.get_n_targets():
            targets.append(target)
        for target in self.get_e_targets():
            targets.append(target)
        for target in self.get_s_targets():
            targets.append(target)
        for target in self.get_w_targets():
            targets.append(target)
        for target in self.get_nw_targets():
            targets.append(target)
        for target in self.get_ne_targets():
            targets.append(target)
        for target in self.get_se_targets():
            targets.append(target)
        for target in self.get_sw_targets():
            targets.append(target)
        return targets

    def get_n_targets(self):
        targets = self.get_targets_helper(-1, 0)
        return targets

    def get_e_targets(self):
        targets = self.get_targets_helper(0, 1)
        return targets

    def get_s_targets(self):
        targets = self.get_targets_helper(1, 0)
        return targets

    def get_w_targets(self):
        targets = self.get_targets_helper(0, -1)
        return targets

    def get_nw_targets(self):
        targets = self.get_targets_helper(-1, -1)
        return targets

    def get_ne_targets(self):
        targets = self.get_targets_helper(-1, 1)
        return targets

    def get_se_targets(self):
        targets = self.get_targets_helper(1, 1)
        return targets

    def get_sw_targets(self):
        targets = self.get_targets_helper(1, -1)
        return targets

    def get_targets_helper(self, x_increment, y_increment):
        targets = []
        next_x = self.x + x_increment
        next_y = self.y + y_increment
        next_cell = (next_x, next_y)
        while board.Board.cell_within_limits(next_x, next_y):
            if self.board.has_piece(next_x, next_y):
                if self.board.cells[next_x][next_y].color != self.color:
                    targets.append(next_cell)
                break
            next_x += x_increment
            next_y += y_increment
            next_cell = (next_x, next_y)
        return targets


class King(Piece):

    def __init__(self, x=0, y=4, board=board.Board(), color=color.Color.WHITE):
        self.x = x
        self.y = y
        self.board = board
        self.board.cells[x][y] = self
        self.board.pieces.append(self)
        self.color = color
        self.has_moved = False

    @property
    def position(self):
        return str((self.x, self.y))

    @property
    def get_letter(self):
        if self.color == color.Color.WHITE:
            return "K"
        else:
            return "k"

    def __str__(self):
        return f"{self.color} king at {self.position}"

    def __repr__(self):
        return f"{self.color} king at {self.position}"

    def get_moves(self):
        move_candidates = self.get_move_candidates()
        available_moves = []
        for move in move_candidates:
            if board.Board.cell_within_limits(move[0], move[1]) and not self.board.has_piece(move[0], move[1]):
                available_moves.append(move)
        return available_moves

    def get_move_candidates(self):
        move_candidates = [(self.x - 1, self.y - 1), (self.x - 1, self.y), (self.x - 1, self.y + 1),
                           (self.x, self.y - 1), (self.x, self.y + 1), (self.x + 1, self.y + 1),
                           (self.x + 1, self.y), (self.x + 1, self.y - 1)]
        return move_candidates

    def get_targets(self):
        available_targets = []
        possible_cells = self.get_move_candidates()
        for cell in possible_cells:
            if board.Board.cell_within_limits(cell[0], cell[1]) and self.board.has_piece(cell[0], cell[1]) and \
                    self.board.cells[cell[0]][cell[1]].color != self.color:
                available_targets.append(cell)
        return available_targets

    def castle(self, target_rook):
        if target_rook is None:
            raise ValueError("No rook to castle with.")
        if target_rook.color != self.color:
            raise ValueError("Can't castle with a rook from opposite color.")
        if not self.is_castling_path_free(target_rook):
            raise ValueError("Castling path is blocked.")
        if self.has_moved or target_rook.has_moved:
            raise ValueError("Illegal castle attempt. King or rook has moved.")
        if not self.is_castling_path_check_free(target_rook):
            raise ValueError("Can't castle through check.")
        if target_rook.y == 0:
            self.castle_queenside(target_rook)
        elif target_rook.y == 7:
            self.castle_kingside(target_rook)
        else:
            raise ValueError("Illegal castle attempt")

    def get_target_rook_by_target_cell(self, x, y):
        if y == 6:
            return self.board.get_piece_by_cell(self.x, 7)
        elif y == 2:
            return self.board.get_piece_by_cell(self.x, 0)
        else:
            return None

    def can_castle(self, target_rook):
        if target_rook is None:
            return False
        if target_rook.color != self.color:
            return False
        if not self.is_castling_path_free(target_rook):
            return False
        if self.has_moved or target_rook.has_moved:
            return False
        if not self.is_castling_path_check_free(target_rook):
            return False
        if target_rook.y != 0 and target_rook.y != 7:
            return False
        return True

    def castle_kingside(self, target_rook):
        self.place(self.x, 6)
        target_rook.place(self.x, 5)

    def castle_queenside(self, target_rook):
        self.place(self.x, 2)
        target_rook.place(self.x, 3)

    def is_castling_path_free(self, target_rook):
        x = self.x
        y = target_rook.y
        if y > 4:
            for i in range(5, y):
                if self.board.has_piece(x, i):
                    return False
        else:
            for i in range(3, y, -1):
                if self.board.has_piece(x, i):
                    return False
        return True

    def is_castling_path_check_free(self, target_rook):
        starting_x = self.x
        starting_y = self.y
        if target_rook.y == 0:
            target_y = 2
            for y in range(starting_y, target_y - 1, -1):
                if self.board.is_cell_under_check(starting_x, y, self.color):
                    return False
        elif target_rook.y == 7:
            target_y = 6
            for y in range(starting_y, target_y + 1):
                if self.board.is_cell_under_check(starting_x, y, self.color):
                    return False
        return True
