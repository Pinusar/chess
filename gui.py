import tkinter as tk
from PIL import ImageTk, Image
import player as plr
import color as clr
import pieces
import board as brd
import game as gm

board = brd.Board()
player1 = plr.Player(color=clr.Color.WHITE)
player2 = plr.Player(color=clr.Color.BLACK)
game = gm.Game(board, player1, player2)
game.reset()


window = tk.Tk()

board = tk.Frame()
board.pack()

letters = [" ", "A", "B", "C", "D", "E", "F", "G", "H"]

cells = []

click_count = 0

for i in range(9):
    frame = tk.Frame(master=board, width=40, height=40, borderwidth=7)
    lbl = tk.Label(master=frame, text=f"       {letters[i]}")
    frame.grid(row=0, column=i)
    lbl.pack()

img = ImageTk.PhotoImage(Image.open("img/white_pawn.gif"))


def select_cell(input):
    result = brd.Board.get_chess_notation(input[0], input[1])
    entry_move.insert(tk.END, f"{result} ")
    global click_count
    if click_count == 1:
        execute_move()
    else:
        click_count += 1


for i in range(1, 9):
    row = i - 1
    frame = tk.Frame(master=board, width=40, height=40)
    lbl = tk.Label(master=frame, text=f"       {i}")
    frame.grid(row=i, column=0)
    lbl.pack()
    cells.append([])
    for j in range(1, 9):
        column = j - 1
        frame = tk.Frame(master=board, width=40, height=40)
        if (i + j) % 2 == 0:
            color = "#f0EFAD"
        else:
            color = "brown"
        piece = game.board.get_piece_by_cell(row, column)
        # if type(piece) is pieces.Pawn:
        #
        #     btn = tk.Button(width=90, height=90, bg=color, master=frame, image=img)
        # else:
        btn = tk.Button(width=5, height=3, bg=color, master=frame, name=f"{i}, {j}", font=("courier", 18),
                        text=f"{brd.Board.get_piece_gui_repr(piece)}", command= lambda c=(row, column): select_cell(c))
        frame.grid(row=i, column=j)
        btn.pack()
        cells[row].append(btn)

# panel = tk.Label(master=window, image=img)
# panel.pack(side = "bottom", fill = "both", expand = "yes")

def reset():
    game.reset()

def execute_move():
    move_input = entry_move.get()
    entry_move.delete(0, tk.END)
    global click_count
    click_count = 0
    parts = move_input.split(" ")
    starting_cell = brd.Board.get_cell(parts[0])
    starting_x = starting_cell[0]
    starting_y = starting_cell[1]
    target_cell = brd.Board.get_cell(parts[1])
    target_x = target_cell[0]
    target_y = target_cell[1]
    piece = game.board.get_piece_by_cell(starting_x, starting_y)
    game.play_turn_with_move(move_input)
    if type(piece) is pieces.King and abs(starting_y - target_y) > 1:
        print("Castling point")
        cells[target_x][target_y]["text"] = cells[starting_x][starting_y]["text"]
        cells[starting_x][starting_y]["text"] = " "
        if target_y == 6:
            cells[target_x][5]["text"] = cells[starting_x][7]["text"]
            cells[starting_x][7]["text"] = " "
        else:
            cells[target_x][3]["text"] = cells[starting_x][0]["text"]
            cells[starting_x][0]["text"] = " "
    else:
        cells[target_x][target_y]["text"] = cells[starting_x][starting_y]["text"]
        cells[starting_x][starting_y]["text"] = " "


lbl_move_field = tk.Label(master=window, text="Please enter your move:")
lbl_move_field.pack()

entry_move = tk.Entry(master=window)
entry_move.pack()

btn_move = tk.Button(master=window, text="Execute move", command=execute_move)
btn_move.pack()


btn_reset = tk.Button(master=window, text="Restart game", command=reset)
btn_reset.pack()



window.mainloop()